
const page = document.querySelector('.page');
const colorTheme = document.querySelector('.colorTheme');

if (!!localStorage.getItem('colorTheme')) {
    page.classList.toggle('orange-theme');
    colorTheme.innerText = 'White Theme';
} else {
    colorTheme.innerText = 'Orange Theme';
};


colorTheme.addEventListener('click', e => {
    if (!!localStorage.getItem('colorTheme')) {
        localStorage.removeItem('colorTheme');
        page.classList.toggle('orange-theme');
        colorTheme.innerText = 'Orange Theme';
    } else {
        localStorage.setItem('colorTheme', 'true');
        page.classList.toggle('orange-theme');
        colorTheme.innerText = 'White Theme';
    }
});

